<?php

namespace App\Models\AGS;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sp2rc extends Model
{
    use HasFactory;
    protected $connection = 'ags_mysql';
    protected $table = 'sp2rc';
    protected $guarded = ['id'];
}
