<?php

namespace App\Http\Controllers\Api\AGS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AGS\Sp2rc;

class Sp2RcController extends Controller
{
    public function create(Request $request)
    {
        $data = json_decode($request->getContent());
        $sp2rc = new Sp2rc;
        $sp2rc->create([
            "container_match" => $data->container_match,
            "procedure" => $data->procedure,
            "terbit" => $data->terbit,
            "expired" => $data->expired,
        ]);
        return json_encode([
            'status' => 'success',
            'req' => $data,
            'res' => 'undesc',
        ]);
    }
}
